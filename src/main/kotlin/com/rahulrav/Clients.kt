package com.rahulrav

import com.rahulrav.futures.Future

class BasicClient {
  sealed class Result {
    object Error : Result()
    class Success(val contents: String) : Result()
  }

  private lateinit var logger: org.slf4j.Logger

  constructor() {
    logger = org.slf4j.LoggerFactory.getLogger(BasicClient::class.java)
  }

  fun handleRequest(query: String): Future<Result> {
    return Future.submit {
      try {
        // just simulate an HTTP request or an API call.
        val timeToSleep = (Math.random() * 100 + 1).toLong();
        Thread.sleep(timeToSleep)
        Result.Success("Hello $query")
      } catch (exception: Exception) {
        logger.error("Unable to handler request ${exception.message} (${exception.cause})", exception)
        Result.Error
      }
    }
  }
}
